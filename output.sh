#!/bin/bash

for file in ./2017/*
do
zless "$file" | ./tools/tweet2tab text >> 2017.txt
done

for file in ./2016/*
do
zless "$file" | ./tools/tweet2tab text >> 2016.txt
done

for file in ./2015/*
do
zless "$file" | ./tools/tweet2tab text >> 2015.txt
done

for file in ./2014/*
do
zless "$file" | ./tools/tweet2tab text >> 2014.txt
done

for file in ./2013/*
do
zless "$file" | ./tools/tweet2tab text >> 2013.txt
done
