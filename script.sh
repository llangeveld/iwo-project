#!/bin/bash
# Total amount of tweets-pipe: get text | count lines
# Find amount of abbreviations: get text | find the word in the text
# grep options used: w - count only the entire word (just 'omg' instead of also 'omgeving')
#                    i - case insensitive
#                    c - returns the number of times the word is found
# There is an empty echo between the years, so there's an empty line between the years. This is for clearer output formatting.

# ---- 2013 -----
tweet13=`cat 2013.txt | wc -l`
echo Number of tweets in 2013: $tweet13
omg13=`cat 2013.txt | grep -wic 'omg'`
echo Number of OMG\'s in 2013: $omg13
wss13=`cat 2013.txt | grep -wic 'wss'`
echo Number of wss\'s in 2013: $wss13
lol13=`cat 2013.txt | grep -wic 'lol'`
echo Number of lol\'s in 2013: $lol13
sws13=`cat 2013.txt | grep -wic 'sws'`
echo Number of sws\'s in 2013: $sws13
idd13=`cat 2013.txt | grep -wic 'idd'`
echo Number of idd\'s in 2013: $idd13


echo

# ---- 2014 ----
tweet14=`cat 2014.txt | wc -l`
echo Number of tweets in 2014: $tweet14
omg14=`cat 2014.txt | grep -wic 'omg'`
echo Number of OMG\'s in 2014: $omg14
wss14=`cat 2014.txt | grep -wic 'wss'`
echo Number of wss\'s in 2014: $wss14
lol14=`cat 2014.txt | grep -wic 'lol'`
echo Number of lol\'s in 2014: $lol14
sws14=`cat 2014.txt | grep -wic 'sws'`
echo Number of sws\'s in 2014: $sws14
idd14=`cat 2014.txt | grep -wic 'idd'`
echo Number of idd\'s in 2014: $idd14


echo

# ---- 2015 ----
tweet15=`cat 2015.txt | wc -l`
echo Number of tweets in 2015: $tweet15
omg15=`cat 2015.txt | grep -wic 'omg'`
echo Number of OMG\'s in 2015: $omg15
wss15=`cat 2015.txt | grep -wic 'wss'`
echo Number of wss\'s in 2015: $wss15
lol15=`cat 2015.txt | grep -wic 'lol'`
echo Number of lol\'s in 2015: $lol15
sws15=`cat 2015.txt | grep -wic 'sws'`
echo Number of sws\'s in 2015: $sws15
idd15=`cat 2015.txt | grep -wic 'idd'`
echo Number of idd\'s in 2015: $idd15

echo

# ---- 2016 ----
tweet16=`cat 2016.txt | wc -l`
echo Number of tweets in 2016: $tweet16
omg16=`cat 2016.txt | grep -wic 'omg'`
echo Number of OMG\'s in 2016: $omg16
wss16=`cat 2016.txt | grep -wic 'wss'`
echo Number of wss\'s in 2016: $wss16
lol16=`cat 2016.txt | grep -wic 'lol'`
echo Number of lol\'s in 2016: $lol16
sws16=`cat 2016.txt | grep -wic 'sws'`
echo Number of sws\'s in 2016: $sws16
idd16=`cat 2016.txt | grep -wic 'idd'`
echo Number of idd\'s in 2016: $idd16


echo

# ---- 2017 ----
tweet17=`cat 2017.txt | wc -l`
echo Number of tweets in 2017: $tweet17
omg17=`cat 2017.txt | grep -wic 'omg'`
echo Number of OMG\'s in 2017: $omg17
wss17=`cat 2017.txt | grep -wic 'wss'`
echo Number of wss\'s in 2017: $wss17
lol17=`cat 2017.txt | grep -wic 'lol'`
echo Number of lol\'s in 2017: $lol17
sws17=`cat 2017.txt | grep -wic 'sws'`
echo Number of sws\'s in 2017: $sws17
idd17=`cat 2017.txt | grep -wic 'idd'`
echo Number of idd\'s in 2017: $idd17



