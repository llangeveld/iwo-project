#!/bin/bash
# Lonneke Langeveld
# 12-03-2017
# This script works with Tweets found in the twitter2 corpus.

echo Amount of tweets:
amount=`zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | wc -l`
echo $amount
# It echoes in two parts, so the user can see what the computer is working on.
# Explanation: get file | count the amount of lines in the file.

echo Amount of unique tweets:
unique=`zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | wc -l`
echo $unique
# Explanation: get file | get the text from the tweets | sort file | remove duplicates | count lines

echo Amount of retweets:
retweet=`zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | grep ^RT | wc -l`
echo $retweet
# Explanation: get file | get the text | sort file | find lines that start with 'RT' | count lines

echo The first 20 unique tweets:
first=`zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | head -n20`
echo $first
# Explanation: get file | get the text | sort file | remove duplicates | get the first 20 lines
